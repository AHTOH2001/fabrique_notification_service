from concurrent.futures import ThreadPoolExecutor
import json
import os
from time import sleep

import django
import pika
from django.conf import settings
from django.utils import timezone


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "notification_service.settings")
django.setup()


def on_message_received(channel, method, properties, body):
    from main_app.models import Client, Mailing, Message

    data = json.loads(body)
    print(f"received new message: {data}")
    mailing = Mailing.objects.get(pk=data["mailing_pk"])
    client_op_code, client_tag = mailing.filter.split(".")
    clients = Client.objects.filter(operator_code=client_op_code).union(
        Client.objects.filter(tag=client_tag)
    )
    messages = Message.objects.bulk_create(
        [
            Message(status=Message.Statuses.CREATED, mailing=mailing, receiver=client)
            for client in clients
        ]
    )

    with ThreadPoolExecutor() as pool:

        def message_sender(message):
            if message.mailing.end_time < timezone.now():
                message.status = Message.Statuses.EXPIRED
            else:
                message.status = Message.Statuses.SENDING
                message.save(update_fields=["status"])
                # TODO actual sending
                sleep(5)
                message.status = Message.Statuses.SENT

            message.save(update_fields=["status"])

        pool.map(message_sender, messages, timeout=60)


settings.RQ_CHANNEL.basic_consume(
    queue=settings.RQ_QUEUE,
    auto_ack=True,
    on_message_callback=on_message_received,
    exclusive=True,
)

print("Starting Consuming")

settings.RQ_CHANNEL.start_consuming()

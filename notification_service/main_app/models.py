import json
import os
from datetime import timedelta
from multiprocessing import Process, Queue

import pika
from django.conf import settings
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin
from timezone_field import TimeZoneField


class Mailing(ExportModelOperationsMixin("Mailing"), models.Model):
    start_time = models.DateTimeField()
    message = models.CharField(max_length=255)
    filter = models.CharField(max_length=255)  # TODO think about format
    end_time = models.DateTimeField()

    def clean(self):
        if self.start_time > self.end_time:
            raise ValidationError("Start time is after end time")

    @staticmethod
    def on_created(sender, instance, created, raw, using, update_fields, **kwargs):
        if created:
            instance = Mailing.objects.get(pk=instance.pk)
            body = {"mailing_pk": instance.pk}

            if instance.start_time > timezone.now():
                delta: timedelta = instance.start_time - timezone.now()
                delay = int(delta.total_seconds() * 1000)
            else:
                delay = 0

            settings.RQ_CHANNEL.basic_publish(
                exchange=settings.RQ_EXCHANGE,
                routing_key=settings.RQ_QUEUE,
                body=json.dumps(body),
                properties=pika.BasicProperties(headers={"x-delay": delay}),
            )

    def __str__(self) -> str:
        return f"Mailing {self.pk}"


class Client(ExportModelOperationsMixin("Client"), models.Model):
    phone = models.BigIntegerField(
        validators=[
            validators.MinValueValidator(10**11),
            validators.MaxValueValidator(10**12 - 1),
        ],
        unique=True,
    )
    operator_code = models.IntegerField()
    tag = models.CharField(max_length=100)
    time_zone = TimeZoneField()

    def __str__(self) -> str:
        return f"Client {self.pk}"


class Message(ExportModelOperationsMixin("Message"), models.Model):
    class Statuses(models.TextChoices):
        CREATED = "created"
        SENDING = "sending"
        SENT = "sent"
        EXPIRED = "expired"

    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=20, choices=Statuses.choices, default=Statuses.CREATED
    )
    mailing = models.ForeignKey(
        Mailing,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name="messages",
    )
    receiver = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="messages"
    )

    def __str__(self) -> str:
        return f"Message {self.pk}"


post_save.connect(Mailing.on_created, sender=Mailing)

from datetime import datetime
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from django.utils import timezone

from main_app.models import Client, Mailing
from main_app.serializers import ClientSerializer, MailingSerializer


class ClientTests(APITestCase):
    def setUp(self) -> None:
        self.data = {
            "phone": 712345678912,
            "operator_code": 15,
            "tag": "xyz",
            "time_zone": "UTC",
        }

    def test_post_client(self):
        url = "/client/"

        response = self.client.post(url, self.data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Client.objects.count(), 1)

        client = Client.objects.all().first()
        self.assertEqual(ClientSerializer(client).data, self.data)

    def test_get_client(self):
        old_client = Client.objects.create(**self.data)

        url = f"/client/{old_client.pk}/"

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.data, response.data)

    def test_put_client(self):
        old_client = Client.objects.create(**self.data)

        url = f"/client/{old_client.pk}/"
        new_data = {
            "phone": 799988877766,
            "operator_code": 77,
            "tag": "op",
            "time_zone": "America/New_York",
        }

        response = self.client.put(url, new_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        client = Client.objects.all().first()
        self.assertEqual(ClientSerializer(client).data, new_data)

    def test_patch_client(self):
        old_client = Client.objects.create(**self.data)

        url = f"/client/{old_client.pk}/"
        new_data = {
            "phone": 799988877766,
        }

        response = self.client.patch(url, new_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        client = Client.objects.all().first()
        self.assertEqual(ClientSerializer(client).data, self.data | new_data)

    def test_delete_client(self):
        old_client = Client.objects.create(**self.data)

        url = f"/client/{old_client.pk}/"

        response = self.client.delete(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Client.objects.count(), 0)


class MailingTests(APITestCase):
    def setUp(self) -> None:
        self.data = {
            "start_time": "2022-08-25T18:00:00Z",
            "message": "test message",
            "filter": "15.xyz",
            "end_time": "2022-08-25T19:00:00Z",
        }

    def test_post_mailing(self):
        url = "/mailing/"

        response = self.client.post(url, self.data, format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Mailing.objects.count(), 1)

        mailing = Mailing.objects.all().first()
        self.assertEqual(MailingSerializer(mailing).data, self.data)

    def test_get_mailing(self):
        old_mailing = Mailing.objects.create(**self.data)

        url = f"/mailing/{old_mailing.pk}/"

        response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.data, response.data)

    def test_put_mailing(self):        
        old_mailing = Mailing.objects.create(**self.data)

        url = f"/mailing/{old_mailing.pk}/"
        new_data = {
            "start_time": "2021-07-20T18:00:00Z",
            "message": "new test message",
            "filter": "22.abc",
            "end_time": "2021-09-21T18:00:00Z",
        }

        response = self.client.put(url, new_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mailing = Mailing.objects.all().first()
        self.assertEqual(MailingSerializer(mailing).data, new_data)

    def test_patch_mailing(self):
        old_mailing = Mailing.objects.create(**self.data)

        url = f"/mailing/{old_mailing.pk}/"
        new_data = {
            "message": "another message",
        }

        response = self.client.patch(url, new_data, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        mailing = Mailing.objects.all().first()
        self.assertEqual(MailingSerializer(mailing).data, self.data | new_data)

    def test_delete_mailing(self):
        old_mailing = Mailing.objects.create(**self.data)

        url = f"/mailing/{old_mailing.pk}/"

        response = self.client.delete(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Mailing.objects.count(), 0)

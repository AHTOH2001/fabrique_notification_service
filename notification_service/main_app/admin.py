from django.contrib import admin

from main_app.models import *


class ClientAdmin(admin.ModelAdmin):
    search_fields = ["phone", "operator_code", "tag"]
    list_display = [
        "phone",
        "operator_code",
        "tag",
        "time_zone",
    ]
    list_filter = ["operator_code", "time_zone"]


class MailingAdmin(admin.ModelAdmin):
    search_fields = ["message"]
    list_display = [
        "message",
        "start_time",
        "end_time",
        "filter",
    ]
    list_filter = ["start_time"]


class MessageAdmin(admin.ModelAdmin):
    search_fields = [
        "receiver__phone",
        "receiver__operator_code",
        "receiver__tag",
        "mailing__message",
    ]
    list_display = [
        "mailing",
        "receiver",
        "created_at",
        "status",
    ]
    list_filter = ["status", "created_at"]


admin.site.register(Client, ClientAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Message, MessageAdmin)

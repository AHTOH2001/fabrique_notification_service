from rest_framework import serializers
from timezone_field.rest_framework import TimeZoneSerializerField

from main_app.models import *


class ClientSerializer(serializers.ModelSerializer):
    time_zone = TimeZoneSerializerField()

    class Meta:
        model = Client
        fields = ["phone", "operator_code", "tag", "time_zone"]


class MailingSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if "start_time" in attrs and "end_time" in attrs:
            if attrs["start_time"] > attrs["end_time"]:
                raise serializers.ValidationError("Start time is after end time")
        return attrs

    class Meta:
        model = Mailing
        fields = ["start_time", "message", "filter", "end_time"]

from django.urls import path
from rest_framework.routers import DefaultRouter

from main_app.views import *


router = DefaultRouter()
router.register("client", ClientViewSet, "client")
router.register("mailing", MailingViewSet, "mailing")

urlpatterns = []

urlpatterns.extend(router.urls)
